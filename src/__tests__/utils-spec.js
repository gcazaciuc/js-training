// Here goes the unit tests for utils.js
const Utils = require('../utils.js');

describe('Utils suite', function() {
    it('Should map an array of values', function() {
        // Access any defined function by Utils.functionName
        const result = Utils.map([1,2], (x) => x*2);
        expect(result).toEqual([2, 4]);
    });
});